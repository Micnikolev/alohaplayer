//
//  VideoPathFactoryTests.swift
//  AlohaPlayerTests
//
//  Created by Mikhail Nikolaev on 06/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

@testable import AlohaPlayer
import XCTest

final class VideoPathFactoryTests: XCTestCase {
    let videoPathFactory = VideoPathFactory()
    
    override func setUp() {
        super.setUp()
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.downloadedVideoUrlsKey)
    }
    
    override func tearDown() {
        super.tearDown()
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.downloadedVideoUrlsKey)
    }
    
    func testMakeDownloadedVideoPathWhenSavedVideoExists() {
        let path = "https://clips.vorwaerts-gmbh.de/VfE_html5.mp4"
        let url = URL(string: path)
        let localFileName = "file"
        UserDefaults.standard.set([path: localFileName], forKey: UserDefaultsKeys.downloadedVideoUrlsKey)
        
        let videoPath = videoPathFactory.makeDownloadedVideoPath(with: url!)!
        
        XCTAssertTrue(videoPath.contains(localFileName))
    }
    
    func testMakeDownloadedNotVideoPathWhenSavedVideoNotExist() {
        let path = "https://clips.vorwaerts-gmbh.de/VfE_html5.mp4"
        let url = URL(string: path)
        
        XCTAssertNil(videoPathFactory.makeDownloadedVideoPath(with: url!))
    }
}
