//
//  LocalVideo.swift
//  AlohaPlayer
//
//  Created by Mikhail Nikolaev on 04/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

import Foundation

/// Video that is loaded and saved from the web
struct LocalVideo {
    /// Name of the video
    let localName: String
    
    /// Path which is based on url form the web
    let webPath: String
}
