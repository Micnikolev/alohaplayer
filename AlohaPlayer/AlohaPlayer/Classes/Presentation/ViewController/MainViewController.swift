//
//  MainViewController.swift
//  AlohaPlayer
//
//  Created by Mikhail Nikolaev on 04/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVFoundation

/// Controller with the Video Player
final class MainViewController: UIViewController {
    
    // MARK: - Private Properties
    
    @IBOutlet private weak var downloadButton: UIButton!
    @IBOutlet private weak var playerView: UIView!
    @IBOutlet private weak var playerStackView: UIStackView!
    @IBOutlet private weak var videoURLTextField: UITextField!
    @IBOutlet private weak var videoProgressView: UIProgressView!
    
    private var playerLayer = AVPlayerLayer(player: nil)
    private var viewModel = MainViewModel()
    private let disposeBag = DisposeBag()
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videoProgressView.isHidden = true

        playerLayer = AVPlayerLayer(player: viewModel.player.value)
        playerLayer.videoGravity = .resizeAspectFill
        playerView.layer.addSublayer(playerLayer)

        videoURLTextField.rx.text
            .orEmpty
            .bind(to: viewModel.videoPath)
            .disposed(by: disposeBag)

        viewModel
            .player
            .asObservable()
            .subscribe(onNext: { [weak self] player in
                self?.playerLayer.player = player
            }).disposed(by: disposeBag)

        viewModel
            .isVideoFound
            .asObservable()
            .subscribe(onNext: { [weak self] isVideoFound in
                DispatchQueue.main.async {
                    self?.playerStackView.isHidden = !isVideoFound.0
                    self?.downloadButton.isHidden = isVideoFound.1
                }
            }).disposed(by: disposeBag)

        videoURLTextField.rx.controlEvent(.editingDidEndOnExit)
            .subscribe(onNext: { [weak self] _ in
                self?.downloadButton.isHidden = false
                self?.viewModel.updateVideoPlayer(isPlaying: false)
            }).disposed(by: disposeBag)

        viewModel
            .downloadedProgress
            .asObservable()
            .subscribe(onNext: { [weak self] downloadedProgress in
                DispatchQueue.main.async {
                    self?.videoProgressView.progress = downloadedProgress
                    self?.videoProgressView.isHidden = downloadedProgress == 1
                }
            }).disposed(by: disposeBag)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = playerView.layer.bounds
    }
    
    // MARK: - Private methods
    
    private func showNotSupportedFormatForDownloadAlert() {
        let alert = UIAlertController(
            title: "Sorry",
            message: "Not supported video format for download",
            preferredStyle: .alert)
        
        let cancel = UIAlertAction(
            title: "Ok",
            style: .cancel,
            handler: nil)
        
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func playButtonPressed(_ sender: PlayButton) {
        viewModel.updatePlayButton(sender.isPlaying)
    }
    
    @IBAction func downloadButtonPressed(_ sender: Any) {
        guard viewModel.downloadVideo() else {
            showNotSupportedFormatForDownloadAlert()
            return
        }
        downloadButton.isHidden = true
    }
}
