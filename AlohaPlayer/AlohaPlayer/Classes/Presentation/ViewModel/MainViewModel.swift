//
//  MainViewModel.swift
//  AlohaPlayer
//
//  Created by Mikhail Nikolaev on 04/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

import Foundation
import RxSwift
import AVFoundation

final class MainViewModel: NSObject {
    
    // MARK: - Public Properties
    
    /// Web path for Video
    let videoPath = Variable("")
    
    /// (Founded in web, Founded locally)
    var isVideoFound = Variable((false, false))
    
    /// Player for playing video
    var player = Variable(AVPlayer(playerItem: nil))
    
    /// Progress of downloading video
    var downloadedProgress = Variable(Float(1.0))
    
    // MARK: - Private Properties
    
    private let videoService = VideoService()
    private let videoPathFactory = VideoPathFactory()
    private let notSupportedDownloadedFormats = ["m3u8"]
    
    override init() {
        super.init()
        videoService.setupDownloadingStatusDelegate(delegate: self)
    }
    
    // MARK: - Public Methods
    
    func updateVideoPlayer(isPlaying: Bool) {
        updatePlayButton(isPlaying)
        
        guard
            var videoURL = URL(string: videoPath.value),
            !videoURL.pathExtension.isEmpty
            else {
                isVideoFound.value = (false, false)
                return
        }
        if let path = videoPathFactory.makeDownloadedVideoPath(with: videoURL) {
            videoURL = URL(string: path)!
            isVideoFound.value = (true, true)
        } else {
            isVideoFound.value = (true, false)
        }
        let asset = AVAsset(url: videoURL)
        let playerItem = AVPlayerItem(asset: asset)
        player.value = AVPlayer(playerItem: playerItem)
        player.value.actionAtItemEnd = .none
        player.value.play()
    }
    
    func downloadVideo() -> Bool {
        guard
            let videoURL = URL(string: videoPath.value),
            !notSupportedDownloadedFormats.contains(videoURL.pathExtension)
            else {
                return false
        }
        downloadedProgress.value = 0.0
        videoService.download(with: videoURL)
        return true
    }
    
    func updatePlayButton(_ isPlaying: Bool) {
        isPlaying ? player.value.pause() : player.value.play()
    }
}

// MARK: - URLSessionDownloadDelegate

extension MainViewModel: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard
            let fileName = downloadTask.response?.suggestedFilename,
            let videoURL = videoPathFactory.makeLocalURL(for: fileName)
            else { return }
        videoService.saveVideo(from: location, to: videoURL)
        videoService.saveUrlOfDownloadedVideo(LocalVideo(localName: fileName, webPath: videoPath.value))
        downloadedProgress.value = 1.0
    }
    
    func urlSession(
        _ session: URLSession,
        downloadTask: URLSessionDownloadTask,
        didWriteData bytesWritten: Int64,
        totalBytesWritten: Int64,
        totalBytesExpectedToWrite: Int64) {
        downloadedProgress.value = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
    }
}
