//
//  PlayButton.swift
//  AlohaPlayer
//
//  Created by Mikhail Nikolaev on 04/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

import UIKit

/// Button for Play/Pause video
final class PlayButton: UIButton {
    private let playButtonImage = UIImage(named: "playButton")
    private let pauseButtonImage = UIImage(named: "pauseButton")
    
    private(set) var isPlaying = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
    }
    
    @objc func buttonSelected() {
        isPlaying = !isPlaying
        let image = isPlaying ? pauseButtonImage : playButtonImage
        setImage(image, for: .normal)
    }
}
