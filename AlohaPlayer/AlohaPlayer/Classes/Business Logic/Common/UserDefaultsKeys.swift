//
//  UserDefaultsKeys.swift
//  AlohaPlayer
//
//  Created by Mikhail Nikolaev on 06/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

import Foundation

final class UserDefaultsKeys {
    static let downloadedVideoUrlsKey = "downloadedVideoUrls"
}
