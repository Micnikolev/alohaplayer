//
//  VideoPathFactory.swift
//  AlohaPlayer
//
//  Created by Mikhail Nikolaev on 06/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

import Foundation

final class VideoPathFactory {
    
    // MARK: - Public methods
    
    func makeDownloadedVideoPath(with url: URL) -> String? {
        guard let urls = UserDefaults.standard.dictionary(forKey: UserDefaultsKeys.downloadedVideoUrlsKey) else {
            return nil
        }
        
        if let videoName = urls[url.absoluteString] as? String,
            let localURL = makeLocalURL(for: videoName) {
            return localURL.absoluteString
        }
        
        return nil
    }
    
    func makeLocalURL(for videoName: String) -> URL? {
        guard
            let documentsPath = NSSearchPathForDirectoriesInDomains(
                .documentDirectory, .userDomainMask, true)
                .first else { return nil }
        let documentsURL = URL(fileURLWithPath: documentsPath)
        let videoURL = documentsURL.appendingPathComponent(videoName)
        return videoURL
    }
}
