//
//  VideoService.swift
//  AlohaPlayer
//
//  Created by Mikhail Nikolaev on 04/01/2019.
//  Copyright © 2019 Mikhail Nikolaev. All rights reserved.
//

import Foundation

/// Service for downloading and saving videos
final class VideoService: NSObject {
    
    // MARK: - Private properties
    
    private var session = URLSession.shared
    private let downloadVideoSessionIdentifier = "downloadVideoSession"
    
    // MARK: - Public methods
    
    func setupDownloadingStatusDelegate(delegate: URLSessionDownloadDelegate) {
        let queue = OperationQueue()
        let config = URLSessionConfiguration.background(withIdentifier: downloadVideoSessionIdentifier)
        session = URLSession(configuration: config, delegate: delegate, delegateQueue: queue)
    }
    
    func download(with url: URL) {
        let downloadTask = session.downloadTask(with: URLRequest(url: url))
        downloadTask.resume()
    }
    
    func saveVideo(from sourceUrl: URL, to destinationUrl: URL) {
        do {
            try FileManager.default.moveItem(at: sourceUrl, to: destinationUrl)
        } catch {
            print(error)
        }
    }
    
    func saveUrlOfDownloadedVideo(_ video: LocalVideo) {
        if var urls = UserDefaults.standard.dictionary(forKey: UserDefaultsKeys.downloadedVideoUrlsKey) {
            urls[video.webPath] = video.localName
            UserDefaults.standard.set(urls, forKey: UserDefaultsKeys.downloadedVideoUrlsKey)
            return
        }
        UserDefaults.standard.set([video.webPath: video.localName], forKey: UserDefaultsKeys.downloadedVideoUrlsKey)
    }

}
